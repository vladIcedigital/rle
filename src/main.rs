/*

Encoding:
Input: Number sequence
Output: Binary sequence

Decoding:
Input: Number sequence
Output: Binary sequence

*/
use std::fmt::Write;
use std::io;
use std::io::*;

#[cfg(test)]
mod tests {
    use super::*;
    #[test]

    fn binary_test() {
        assert_eq!(&encode_binary(10), &1010u16);
        assert_eq!(&encode_binary(8), &1000u16);
        assert_eq!(&encode_binary(3), &11u16);
    }

    #[test]
    fn rle_test() {
        assert_eq!(
            &encode_rle(String::from("00000000")).0,
            &String::from("10000")
        );
        assert_eq!(
            &encode_rle(String::from("10110000")).0,
            &String::from("0011001001011000")
        );
        assert_eq!(
            &encode_rle(String::from("123333333332")).0,
            &String::from("000101000110100111000110")
        );

        assert_eq!(
            &decode_rle(String::from("10000"), 4, 1),
            &String::from("00000000")
        );
        assert_eq!(
            &decode_rle(String::from("0011001001011000"), 3, 1),
            &String::from("10110000")
        );
        assert_eq!(
            &decode_rle(String::from("000101000110100111000110"), 4, 2),
            &String::from("123333333332")
        );
    }
}

fn encode_binary(mut number: u8) -> u16 {
    let mut binary_vector: Vec<u8> = vec![];
    while number != 0 {
        binary_vector.push(number % 2);
        number /= 2;
    }

    let mut binary: u16 = 0;
    for i in 0..binary_vector.len() {
        binary = binary * 10 + binary_vector[binary_vector.len() - 1 - i] as u16;
    }
    return binary;
}

fn get_max(nums: &Vec<u16>) -> u16 {
    let mut max = 0;
    for i in 0..nums.len() {
        if nums[i] > max {
            max = nums[i];
        }
    }
    return max;
}

fn get_length(mut num: u16) -> u8 {
    let mut counter = 0;
    if num == 0 {
        return 1;
    }
    while num != 0 {
        num /= 10;
        counter += 1;
    }
    return counter;
}

fn decode_binary(mut num: u16) -> u8 {
    let mut result: u8 = 0;

    let mut pow: u16 = 1;
    while num != 0 {
        result += ((num % 10) * pow as u16) as u8;
        pow *= 2;
        num /= 10;
    }

    return result;
}

fn binary_to_string(number: u16, length: u8) -> String {
    let mut string: String = String::new();
    for _i in 0..length - get_length(number) {
        string.push('0');
    }
    write!(string, "{}", number).expect("Could not write binary");
    return string;
}

fn decode_rle(encoded_message: String, count_width: u16, symbol_width: u16) -> String {
    let mut result = String::new();

    for i in (0..encoded_message.len()).step_by((count_width + symbol_width) as usize) {
        let encoded_length = encoded_message.chars().as_str()[i..i + count_width as usize]
            .parse::<u16>()
            .unwrap();
        let encoded_symbol = encoded_message.chars().as_str()
            [i + count_width as usize..i + (count_width + symbol_width) as usize]
            .parse::<u16>()
            .unwrap();

        let decoded_length = decode_binary(encoded_length);
        let decoded_symbol = decode_binary(encoded_symbol);

        for _n in 0..decoded_length {
            write!(result, "{}", decoded_symbol).expect("Could not write decoded symbol!");
        }
    }
    return result;
}

fn encode_rle(input: String) -> (String, u8, u8) {
    let mut result: (Vec<u16>, Vec<u16>) = (vec![], vec![]);

    let mut current_symbol: i8 = -1;
    let mut counter = 0;

    //filling vector
    {
        for (_index, ch) in input.chars().enumerate() {
            if ch.to_digit(10).unwrap() as i8 == current_symbol {
                counter += 1;
            } else {
                if current_symbol != -1 {
                    result.0.push(encode_binary(current_symbol as u8));
                    result.1.push(encode_binary(counter));
                }
                current_symbol = ch.to_digit(10).unwrap() as i8;
                counter = 1;
            }
        }
        result.0.push(encode_binary(current_symbol as u8));
        result.1.push(encode_binary(counter));
    }

    println!("{:?}", result);

    let max_count_width = get_length(get_max(&result.1));
    let max_symbol_width = get_length(get_max(&result.0));

    println!("Max symbol binary length: {}", max_symbol_width);
    println!("Max count  binary length: {}", max_count_width);

    let mut string_result = String::from("");

    for i in 0..result.1.len() {
        write!(
            string_result,
            "{}{}",
            binary_to_string(result.1[i], max_count_width),
            binary_to_string(result.0[i], max_symbol_width)
        )
        .expect("Could not write binary sequence");
    }
    return (string_result, max_count_width, max_symbol_width);
}

fn main() {
    println!("Enter symbols without spaces (english-only): ");

    let stdin = io::stdin();
    let mut iterator = stdin.lock().lines();
    let string = iterator.next().unwrap().unwrap();
    println!("e/d? (encode/decode)");
    let result = iterator.next().unwrap().unwrap();
    if result == "e" {
        println!("{}", encode_rle(string).0);
    } else if result == "d" {
        println!("symbol_width count_width");
        let widths = iterator.next().unwrap().unwrap();
        let widths: Vec<&str> = widths.split(' ').collect();
        println!(
            "{}",
            decode_rle(
                string,
                widths[1].parse::<u16>().unwrap(),
                widths[0].parse::<u16>().unwrap(),
            )
        );
    }
}
